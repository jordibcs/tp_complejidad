﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_TP_TSP
{
    class ColeccionCP
    {
        public FeatureCollection centrosPoblados()
        {
            using (StreamReader reader = new StreamReader("C:\\Users\\Jordi\\Documents\\UPC\\Ciclo7\\Complejidad Algorítmica\\2018-2\\Trabajo parcial\\Programa\\CA_TP_TSP\\GeoJSON\\CP_P_20180726.json"))
            {
                string jsonString = reader.ReadToEnd();
                FeatureCollection listFeatureCollection = new FeatureCollection();
                listFeatureCollection = JsonConvert.DeserializeObject<FeatureCollection>(jsonString);
                return listFeatureCollection;
            }

        }
    }
}
