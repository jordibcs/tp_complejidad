﻿using GMap.NET;
using GMap.NET.MapProviders;
using GMap.NET.WindowsForms;
using GMap.NET.WindowsForms.Markers;
using GMap.NET.WindowsForms.ToolTips;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CA_TP_TSP
{
    public partial class Form1 : Form
    {
        //Obtener los centros poblados
        ColeccionCP objColleccionCP = new ColeccionCP();
        FeatureCollection cps = new FeatureCollection();
        List<PointLatLng> pointLatLngs = new List<PointLatLng>();

        int n;
        string filtro = "IPSK";

        //Distancia entre dos puntos: d = sqrt((x2-x1)^2 + (y2-y1)^2)
        public double DistanciaPuntos(double x1, double y1, double x2, double y2)
        {
            return Math.Sqrt(Math.Pow((x2 - x1), 2) + Math.Pow((y2 - y1), 2));
        }

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            cps = objColleccionCP.centrosPoblados();
            n = cps.features.Count();

            //Mostrar el mapa
            gmcMap.MapProvider = GMapProviders.EmptyProvider;
            gmcMap.ShowCenter = false;
            gmcMap.MinZoom = 1;
            gmcMap.MaxZoom = 30;
            gmcMap.DragButton = MouseButtons.Left;

            //Personalizar marcador
            Bitmap bmpMarker = (Bitmap)Image.FromFile("C:\\Users\\Jordi\\Documents\\UPC\\Ciclo7\\Complejidad Algorítmica\\2018-2\\Trabajo parcial\\Programa\\CA_TP_TSP\\images\\RedPoint.png");

            //Mostrar los marcadores
            var markers = new GMapOverlay("markers");
            
            for (int i = 0; i < n; i++)
            {
                //if(cps.features[i].properties.nivel.Equals(filtro))
                //{
                    pointLatLngs.Add(new PointLatLng(cps.features[i].properties.ygd, cps.features[i].properties.xgd));
                //}
            }
            foreach (PointLatLng pt in pointLatLngs)
            {
                var marker = new GMarkerGoogle(pt, bmpMarker);
                markers.Markers.Add(marker);
            }

            gmcMap.Overlays.Add(markers);
            gmcMap.ZoomAndCenterMarkers(markers.Id);
        }

        private void btnRuta_Click(object sender, EventArgs e)
        {
            DateTime tInicio = DateTime.Now;
            
            //Para dibujar las lineas
            GMapOverlay polyOverlay = new GMapOverlay("polygons");
            List<PointLatLng> points = new List<PointLatLng>();
            
            cps = objColleccionCP.centrosPoblados();

            var inicio = pointLatLngs[0];
            var destino = pointLatLngs[0];
            List<PointLatLng> visitados = new List<PointLatLng>();
            visitados.Add(inicio);
            
            double men;
            double d = 0;
            double totalDist = 0;

            for (int i = 0; i < pointLatLngs.Count(); i++)
            {
                men = 10000000;
                //Hallando el centro poblado más cercano al centro poblado de origen
                for (int j = 1; j < pointLatLngs.Count(); j++)
                {
                    var cpEvaluado = pointLatLngs[i];
                    if (visitados.Contains(cpEvaluado) == false)
                    {
                        d = DistanciaPuntos(inicio.Lng, inicio.Lat, cpEvaluado.Lng, cpEvaluado.Lat);
                        if (d <= men)
                        {
                            men = d;
                            destino = cpEvaluado;
                        }
                    }
                }

                //Punto de origen y destino para dibujar las lineas
                points.Add(new PointLatLng(inicio.Lat, inicio.Lng));
                points.Add(new PointLatLng(destino.Lat, destino.Lng));

                visitados.Add(destino);
                inicio = destino;
                totalDist += d;
            }

            //Uniendo el punto final con el inicio
            points.Add(new PointLatLng(inicio.Lat, inicio.Lng));
            points.Add(new PointLatLng(pointLatLngs[0].Lat, pointLatLngs[0].Lng));

            //Dibujando las lineas
            var r = new GMap.NET.WindowsForms.GMapRoute(points, "MyRoute");
            r.Stroke.DashStyle = System.Drawing.Drawing2D.DashStyle.Solid;
            r.Stroke.Width = 1;
            polyOverlay = new GMapOverlay("routes");
            polyOverlay.Routes.Add(r);
            gmcMap.Overlays.Add(polyOverlay);

            //Refrescar las lineas para mostrar
            polyOverlay.IsVisibile = false;
            polyOverlay.IsVisibile = true;

            //Hallando el tiempo de demora
            DateTime tFinal = DateTime.Now;
            TimeSpan tDuracion = tFinal - tInicio;
            double tiempoTotal = tDuracion.TotalSeconds;

            //Mostrar distancia total
            lblDistancia.Text = totalDist.ToString();

            //Mostrar tiempo total
            lblTiempo.Text = tiempoTotal.ToString();
        }
    }
}