﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_TP_TSP
{
    public class FeatureCollection
    {
        public string type { get; set; }
        public List<Feature> features { get; set; }
    }

    public class Feature
    {
        public string type { get; set; }
        public Geometry geometry { get; set; }
        public Property properties { get; set; }
    }

    public class Geometry
    {
        public string type { get; set; }
        public List<string> coordinates { get; set; }
    }

    public class Property
    {
        public string ubigeo { get; set; }
        public string dep { get; set; }
        public string prov { get; set; }
        public string dist { get; set; }
        public string codcp { get; set; }
        public string nomcp { get; set; }
        public string mnomcp { get; set; }
        public string capital { get; set; }
        public string con_ie { get; set; }
        public string nivel { get; set; }
        public string cpinei { get; set; }
        public string cpinei2 { get; set; }
        public string cpinei_bkp { get; set; }
        public string fuente_g { get; set; }
        public int z { get; set; }
        public double xgd { get; set; }
        public double ygd { get; set; }
        public string y_x_coord { get; set; }
    }
}
